from django.urls import path
from . import views

urlpatterns=[
    path('',views.user_login,name='user_login'),
    path('register',views.register_obj,name='register'),
    path('user_register',views.user_register,name='user_register'),
    path('book_ticket',views.book_ticket,name='book_ticket')
    #path('user_login',views.user_login,name='user_login')
    # path('update_task/<str:pk>/', views.updateTask,name="update_task"),
    # path('delete/<str:pk>/',views.deleteTask,name="delete"),
]