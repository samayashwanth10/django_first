from django.apps import AppConfig


class MakeMyTripConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'make_my_trip'
