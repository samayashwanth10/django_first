from django.shortcuts import render,redirect
from .models import *
from .forms import *
from django.contrib import auth
from django. contrib import messages
import os

# Create your views here.
def register_obj(request):
    reg_objects=register.objects.all()
    form=TaskForm()
    if request.method == "POST":
        form=TaskForm(request.POST)
        if form.is_valid:
            form.save()
        return redirect('/register')
    
    context = {'tasks':reg_objects, 'form':form}
    return render(request,"make_my_trip/register.html",context)



def user_register(request):
    user_detail=user_details.objects.all()
    form=Userform()
    if request.method == "POST":
        form = Userform(request.POST)
        user=user_detail.filter(email=form['email'].value())
        if user.exists():
            return render(request,"make_my_trip/exceptions.html")
        else:
            if form.is_valid():
                form.save()
            return render(request,"make_my_trip/successfull.html")
    context={'user_detail':user_detail,'form':form}
    return render(request,"make_my_trip/register_user.html",context)

def user_login(request):
    user_detail=user_details.objects.all()
    form=Loginform()
    if request.method=="POST":
        form=Loginform(request.POST)
        user=user_detail.filter(email=form['email'].value(),password=form['password'].value())
        if user.exists():
            messages.info(request, 'valid username or password')
            #return render(request,'make_my_trip/book_ticket')
            return redirect('/book_ticket')
        else:
            messages.info(request, 'invalid username or password')  
            return render(request,"make_my_trip/invalid_login.html")
    context={'user_detail':user_detail,'form':form}
    print("111111")
    return render(request,"make_my_trip/user_login.html",context)


# def home(request):
#     return render(request,"make_my_trip/home.html")
def book_ticket(request):
    details=book_tickets.objects.all()
    form=BookTickets()
    if request.method=="POST":
        form=BookTickets(request.POST)
    context={'user_detail':details,'form':form}
    return render(request,"make_my_trip/book_tickets.html",context)
