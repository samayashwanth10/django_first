from django import forms
# from forms import Modelform
from .models import *

class TaskForm(forms.ModelForm):
    class Meta:
        model=register
        fields='__all__'

class Userform(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model=user_details
        fields='__all__'

class Loginform(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model=user_details
        fields=['email','password']

class BookTickets(forms.ModelForm):
    class Meta:
        model=book_tickets
        fields='__all__'
