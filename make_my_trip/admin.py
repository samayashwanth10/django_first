from django.contrib import admin

# Register your models here.
from .models import *

admin.site.register(register)
admin.site.register(user_details)
admin.site.register(book_tickets)