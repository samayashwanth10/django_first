from django.db import models
import random
import phonenumber_field
from phonenumber_field.modelfields import PhoneNumberField
# Create your models here.
class register(models.Model):
    ID=models.IntegerField(primary_key=True)
    TRAVELS_NAME = models.CharField(max_length=200)
    FROM=models.CharField(max_length=100)
    TO=models.CharField(max_length=100)
    START_TIME=models.TimeField()
    END_TIME =models.TimeField()
    CAPACITY=models.IntegerField()
    COST=models.IntegerField()


    def __str__(self):
        return self.ID
    
class user_details(models.Model):
    email=models.CharField(max_length=200)
    password=models.CharField(max_length=100)
    name=models.CharField(max_length=100)
    contact=PhoneNumberField()
    Native=models.CharField(max_length=100)
    
    def __str__(self):
        field_values = []
        for field in self._meta.get_fields():
            field_values.append(str(getattr(self, field.name, '')))
        return ' '.join(field_values)

class book_tickets(models.Model):
    FROM=models.CharField(max_length=200)
    TO=models.CharField(max_length=100)
    DATE=models.DateField()

    def __str__(self):
        return self.DATE
